# unifi

This is the public-facing side of the UniFi cloud VM. It's a dedicated host, the UniFi management software is not running inside of a Docker container (the plan is to migrate once I have the time).
It uses Caddy as a reverse-proxy (SSL Let's Encrypt is a low-hanging fruit) pointing to the local instance of the UniFi software.
